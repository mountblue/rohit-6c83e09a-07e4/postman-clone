import React, { Component } from "react";
var JSONpretty=require('react-json-pretty')
class HttpResponse extends Component {
    state = {  }
    render() { 
        return (  
            <div style={{height: '29rem', overflow:'auto'}} placeholder='Hit Send to get a response'>{this.props.result.length===0?null:<JSONpretty  data={this.props.result}/>}</div>
        );
    }
}
 
export default HttpResponse;